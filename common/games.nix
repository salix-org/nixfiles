{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    lutris
    wineWowPackages.staging
    winetricks
    # playonlinux
    openttd
    # openrct2
#   multimc
#   unstable.polymc
    prismlauncher
    # freeorion
#   mindustry
    # minetest
    # cataclysm-dda
    # crispyDoom
    # pioneer
    # stellarium
    # retroarch
    # openmw
    heroic
    minecraft
  ];
}
