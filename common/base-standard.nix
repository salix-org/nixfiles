{ pkgs, ... }:

let
  # eps = pkgs.writeScriptBin "eps" ''
  #   #!${pkgs.stdenv.shell}
  #   exec ${pkgs.salix-nur.edit-parent-shell}/bin/main.py
  # '';

  # new-shell = pkgs.callPackage ../packages/new-shell.nix {};
in {
  imports = [
    ../modules/nix.nix
    ../modules/zsh.nix
    ../modules/ssh.nix
    ../modules/fonts.nix
    ../modules/networking.nix
    ../modules/bluetooth.nix
    # ../modules/v4l2-device.nix
    # ../modules/neovim.nix
    ../modules/nixvim.nix
    # ../modules/kitty.nix
    ../modules/git.nix
    # ../modules/salix-nur.nix
  ];

  boot.plymouth.enable = true;

  programs.nix-ld.enable = true;

  programs.tmux.enable = true;

  environment.systemPackages = with pkgs; [
    wget curl bat parted p7zip ranger nnn direnv ripgrep lsof ffmpeg-full nix-tree file fd tree nixos-option
    libva-utils pciutils usbutils psmisc ntfs3g bridge-utils sshfs jmtpfs nixpkgs-fmt
    neofetch htop gotop s-tui powertop dmidecode ncdu
    # eps new-shell
  ];
  nixpkgs.config.allowUnfree = true;
}
