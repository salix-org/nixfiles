{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    teams vscode-fhs
  ];

# environment.shellAliases = {
#   altsrcvpn = "sudo openconnect vpn.altsrc.net:2407";
# };

  # web development, fabscape/ftp ports, etc
# networking.firewall.allowedTCPPorts = [ 8080 8081 44336 5000 3000 9000 9002 20 21 ];
# networking.firewall.allowedTCPPortRanges = [ { from = 25000; to = 25499; } ];

  # vm hostnames
# networking.hosts = {
#   "192.168.122.236" = [ "fabscape-prod-vm.nixos-zephyrus" ];
#   "192.168.10.22" = [ "alt-fabscape" "fabscape-prod-vm.alt-fabscape" ];
# };
}
