{ pkgs, ... }:

let
  godot4beta = pkgs.godot_4.overrideAttrs (finalAttrs: previousAttrs: rec {
    version = "4.2-dev5";
    commitHash = "e3e2528ba7f6e85ac167d687dd6312b35f558591";
    src = pkgs.fetchFromGitHub {
      owner = "godotengine";
      repo = "godot";
      rev = "e3e2528ba7f6e85ac167d687dd6312b35f558591";
      hash = "sha256-0CErsMTrBC/zYcabAtjYn8BWAZ1HxgozKdgiqdsn3q80";
    };
  });
  huggingface-hub-git = pkgs.python311Packages.huggingface-hub.overrideAttrs rec {
    pname = "huggingface-hub-git";
    version = "0.17.2";
    src = pkgs.fetchFromGitHub {
      owner = "huggingface";
      repo = "huggingface_hub";
      rev = "refs/tags/v${version}";
#     hash = pkgs.lib.fakeHash; # apparently this fakeHash exists??
      hash = "sha256-ppp+aYbynFOvUxv16hloRUhmUTpqdLsY545ka1ULD34=";
    };
  };
in {
  # imports = [
  #   ../modules/mpv.nix
  #   ../modules/barrier.nix
  #   ../modules/qutebrowser.nix
  # ];

  environment.systemPackages = with pkgs; [
#   chromium
    brave
    firefox
    signal-desktop
#   discord
#   etcher
    gimp
    # strawberry
    # vscodium
    vscode-fhs
    # zettlr
    remmina
    # handbrake
    vlc
    bitwarden
    # bitwarden-cli
    # nixos-shell
    libreoffice-fresh
#   nextcloud-client
    # dbeaver
    # element-desktop
    # obs-studio
    godot_4
    insomnia
    huggingface-hub-git
    wezterm

#   (callPackage ../packages/parsec.nix {})
#   (callPackage (builtins.fetchurl "https://raw.githubusercontent.com/delroth/infra.delroth.net/master/pkgs/parsec.nix") {})
  ];

  nixpkgs.config.permittedInsecurePackages = [
    "electron-24.8.6"
  ];
# nixpkgs.config.chromium.enableWideVine = true;
# programs.chromium.enable = true;
}
