let
  nixos-hardware = builtins.fetchGit {
    name = "nixos-hardware-git";
    url = "https://github.com/NixOS/nixos-hardware.git";
    #rev = "3cc8c47af31798040ea62499090540413279f832"; # old??
  };
in {
  ga401 = "${nixos-hardware}/asus/zephyrus/ga401";
  precision5530 = "${nixos-hardware}/dell/precision/5530";
  t440p = "${nixos-hardware}/lenovo/thinkpad/t440p";
  acpi_call = "${nixos-hardware}/common/pc/laptop/acpi_call.nix";
}
