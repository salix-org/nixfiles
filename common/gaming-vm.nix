{
  imports = [
    ../common/libvirt.nix
    (import ../modules/vfio-blacklist.nix {
      devices = [ "0000:0c:00.0" "0000:0c:00.1" ];
    })
  ];

  boot.kernelParams = [ "amd_iommu=on" ];

  virtualisation.libvirtd = {
    qemuVerbatimConfig = ''
      namespaces = []
      cgroup_device_acl = [
        "/dev/input/by-id/usb-SONiX_USB_DEVICE-event-kbd",
        "/dev/input/by-id/usb-Logitech_Gaming_Mouse_G502_038538613234-event-mouse",
        "/dev/null", "/dev/full", "/dev/zero",
        "/dev/random", "/dev/urandom",
        "/dev/ptmx", "/dev/kvm", "/dev/kqemu",
        "/dev/rtc", "/dev/hpet", "/dev/sev"
      ]
    '';
  };
}
