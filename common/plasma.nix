{ pkgs, ... }:

{
    services.xserver.displayManager.sddm.enable = true;
    services.xserver.desktopManager.plasma5.enable = true;

    environment.systemPackages = (with pkgs.libsForQt5; [
        ark filelight ksshaskpass qtstyleplugin-kvantum
    ]) ++ (with pkgs; [
        libnotify materia-kde-theme
    ]);

    services.gvfs.enable = true;
    programs.dconf.enable = true;
    programs.kdeconnect.enable = true;
}