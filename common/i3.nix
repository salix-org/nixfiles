{ pkgs, ... }:

{
  imports = [
    ../common/buku-rofi-open.nix
  ];

  services.xserver = {
    windowManager.i3.enable = true;
    windowManager.i3.package = pkgs.i3-gaps;

    displayManager.lightdm.greeters.enso.enable = true;
    displayManager.lightdm.background = let
      salix-wallpaper = pkgs.callPackage ../packages/salix-wallpaper {};
    in "${salix-wallpaper}/lockscreen.jpg";
    displayManager.lightdm.extraSeatDefaults = ''
      greeter-setup-script=${pkgs.numlockx}/bin/numlockx on
    '';
    displayManager.defaultSession = "none+i3";
  };

  services.picom = {
    enable = true;
    fade = true;
    fadeDelta = 4;
    shadow = true;
    shadowExclude = [
      "class_g = 'i3-frame'"
    ];
    opacityRules = [
      "95:class_g = 'kitty'"
      "0:_NET_WM_STATE@[0]:32a *= '_NET_WM_STATE_HIDDEN'"
      "0:_NET_WM_STATE@[1]:32a *= '_NET_WM_STATE_HIDDEN'"
      "0:_NET_WM_STATE@[2]:32a *= '_NET_WM_STATE_HIDDEN'"
      "0:_NET_WM_STATE@[3]:32a *= '_NET_WM_STATE_HIDDEN'"
      "0:_NET_WM_STATE@[4]:32a *= '_NET_WM_STATE_HIDDEN'"
    ];
  };

  services.xrdp = {
    enable = true;
#   openFirewall = true;
    defaultWindowManager = "i3";
  };

  services.autorandr = {
    enable = true;
    defaultTarget = "horizontal";
  };

  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];

  programs.dconf.enable = true;

  programs.nm-applet = {
    enable = true;
#    indicator = false;
  };
  programs.light.enable = true;
  programs.qt5ct.enable = true;

  environment.systemPackages = with pkgs; [
    arandr feh numlockx udiskie dunst maim xclip xdotool lxappearance
    (rofi.override {
      theme = "Pop-Dark";
      plugins = [ rofi-calc ];
    })
    (polybar.override {
      i3GapsSupport = true;
      pulseSupport = true;
    })
  ];

  environment.etc = {
    "xdg/i3/config".source = ../dotfiles/i3/i3config;
    "polybar/config.ini".source = ../dotfiles/polybar/config.ini;
    "polybar/launch_bars.sh" = {
      mode = "0755";
      source = ../dotfiles/polybar/launch_polybar.sh;
    };
    "xdg/dunst/dunstrc".source = ../dotfiles/dunst/dunst.ini;

    # TODO this isn't working, see temporary workaround in ~/.config/autorandr/
    "xdg/autorandr/postswitch" = {
      mode = "0755";
      text = ''
        #!/bin/sh

        # Launch Polybar(s)
        /etc/polybar/launch_bars.sh
      '';
    };
  };
}
