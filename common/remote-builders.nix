{ ... }:

{
  nix.buildMachines = [ {
    hostName = "192.168.1.150";
    systems = [ "x86_64-linux" "aarch64-linux" ];
    maxJobs = 1;
    speedFactor = 2;
    supportedFeatures = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
    mandatoryFeatures = [ ];
  } ];
  nix.distributedBuilds = true;
}
