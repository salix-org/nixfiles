{ config, pkgs, ...}:

{
  hardware.opengl = {
    driSupport32Bit = true;
    extraPackages32 = with pkgs.pkgsi686Linux; [ libva ];
  };

  services.xserver = {
    enable = true;
    exportConfiguration = true;
    dpi = 96; # temp scaling fix until I can figure out the proper solution
    libinput = {
      touchpad.disableWhileTyping = true;
      touchpad.tappingDragLock = false;
      mouse.accelProfile = "flat";
      mouse.accelSpeed = "0.6";
    };
  };

  xdg.portal.enable = true;
}
