let
  share-ip = "192.168.1.238";
in {
  fileSystems."/mnt/netshare" = {
    device = "//${share-ip}/netshare";
    fsType = "cifs";
    options = [
      "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s"
    ];
  };
}
