{ pkgs, ... }:

let
  buku-rofi-open = pkgs.writeScriptBin "buku-rofi-open" ''
    xdg-open $(${pkgs.buku}/bin/buku -p -f 40 | rofi -dmenu -i -p "Search bookmarks" | cut -f1)
  '';
in {
  environment.systemPackages = with pkgs; [ buku buku-rofi-open ];
}
