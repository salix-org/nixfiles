{ config, lib, pkgs, ... }:

let
  rev = "master";
  url = "https://github.com/colemickens/nixpkgs-wayland/archive/${rev}.tar.gz";
  waylandOverlay = (import (builtins.fetchTarball url));
in
  {
    nix.binaryCachePublicKeys = [ "nixpkgs-wayland.cachix.org-1:3lwxaILxMRkVhehr5StQprHdEo4IrE8sRho9R9HOLYA=" ];
    nix.binaryCaches = [ "https://nixpkgs-wayland.cachix.org" ];

    nixpkgs.overlays = [ waylandOverlay ];

    environment.etc = {
      "sway/config".source = ../dotfiles/sway/config;
    };

    programs.sway = {
      enable = true;
      extraPackages = with pkgs; [
        xwayland swaybg swayidle swaylock waybar mako imv wofi kanshi
      ];
    };

    programs.waybar.enable = true;

#    systemd.user.services.kanshi = {
#      description = "Kanshi output daemon";
#      wantedBy = [ "graphical-session.target" ];
#      partOf = [ "graphical-session.target" ];
#      serviceConfig = {
#        ExecStart = ''
#	  ${pkgs.kanshi}/bin/kanshi
#	'';
#	RestartSec = 5;
#	Restart = "always";
#      };
#    };

    services.xserver = {
      displayManager = {
        defaultSession = "sway";
	lightdm.enable = true;
      };
    };
  }
