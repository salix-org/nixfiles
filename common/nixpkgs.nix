{
  # might not be a good idea to import nixos directly until flakes
  # (loses nixpkgs config)
#  stable = import <nixos> {};
#  stable = import <nixos-21.05> { config.allowUnfree = true; };
  unstable = import <nixos-unstable> { config.allowUnfree = true; };
}
