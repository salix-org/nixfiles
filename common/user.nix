{
  users.users.jsalcido = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "video" "lxd" "libvirtd" "qemu-libvirtd" "docker" "adbusers" "input" ];
    initialPassword = "changeme";
  };
}
