{
  virtualisation = {
    libvirtd = {
      enable = true;
      qemu = {
        runAsRoot = false;
        ovmf.enable = true;
      };
      onBoot = "ignore";
      onShutdown = "shutdown";
    };
  };
  users.extraUsers.qemu-libvirtd.extraGroups = [ "input" "kvm" "disk" "libvirtd" ];
}
