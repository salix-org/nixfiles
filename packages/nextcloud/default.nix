{ pkgs }:

pkgs.nextcloud20.overrideAttrs (oldAttrs: {
  patches = [
    ./fix-remote-url.patch
  ];
})
