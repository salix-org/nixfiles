{ stdenv, writeScriptBin, callPackage }:

let
  get-wifi-ipv4 = callPackage ../packages/get-wifi-ipv4.nix {};
in writeScriptBin "barrier-remote-connect" ''
  #!${stdenv.shell}
  client=$(${get-wifi-ipv4}/bin/get-wifi-ipv4)
  ssh "$1" "pkill barrierc; barrierc --enable-crypto $client"
''
