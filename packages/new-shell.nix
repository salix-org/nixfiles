{ writeShellScriptBin, writeText }:

let
  shellnix = writeText "shell.nix" ''
    with import <nixpkgs> {};
    mkShell {
      buildInputs = [
        # PKGS
      ];
    }
  '';
in writeShellScriptBin "new-shell" ''
  cp -i --no-preserve=mode,ownership,timestamps ${shellnix} ./shell.nix
  echo "use nix" > ./.envrc
''
