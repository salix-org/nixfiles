{ fetchurl, python3Packages }:

python3Packages.buildPythonApplication rec {
  pname = "youtube-dlc";
  version = "2020.11.11-3";

  src = fetchurl {
    url = "https://github.com/blackjack4494/yt-dlc/archive/${version}.tar.gz";
    sha256 = "116azzzj0df3bv99zwn0rsarirw56knbq7xqn1fs8v4ilslqp7v4";
  };

  doCheck = false;
}
