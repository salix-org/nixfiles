{ stdenv, writeScriptBin }:

writeScriptBin "get-wifi-ipv4" ''
  #!${stdenv.shell}
  ipv4=$(nmcli -g GENERAL.TYPE,IP4.ADDRESS dev show | rg -A 1 wifi | awk 'NR==2' | cut -d/ -f1)
  if [ -z $ipv4 ]; then
    exit 1
  fi
  echo $ipv4
''
