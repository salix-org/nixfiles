{ stdenv }:

stdenv.mkDerivation {
  pname = "salix-wallpaper";
  version = "0.0.1";

  src = ./.;

  installPhase = ''
    mkdir $out

    cp {*.png,*.jpg} $out/
  '';
}
