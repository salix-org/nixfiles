# Usage

Start with a generic install of NixOS stable. Then:

```
$ cd
$ git clone ...nixfiles ~/nixfiles
$ cd /etc/nixos
$ sudo ln -s ~/nixfiles/machines/[name].nix machine.nix
```

...then import machine.nix from configuration.nix.

AS OF 6/30/23, these configs depend on `nixos-unstable`:

```
$ sudo nix-channel --add https://nixos.org/channels/nixos-unstable nixos
$ sudo nix-channel --update
```

Don't forget to change the default user password after rebooting.

# Pulling Updates

```
$ cd ~/nixfiles
$ git pull
$ sudo nixos-rebuild {switch | boot} [--upgrade-all] [--option extra-substituters ssh://...]
```

# Useful snippets

Set qutebrowser as default browser for xdg-open:

```
$ unset BROWSER
$ xdg-settings set default-web-browser org.qutebrowser.qutebrowser.desktop
```

# Future Thoughts

- ~~Consider switching back to stable and cherrypicking select packages from unstable (some existing config options might prevent this)~~
- Organize and separate the concerns of the system vs user more clearly
- Setup central build server to streamline updates
