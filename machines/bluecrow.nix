{ pkgs, ... }:

{
  imports = [
    ../common/uefi.nix
    ../common/base-standard.nix
    ../common/user.nix
    ../common/xserver.nix
    ../modules/pulseaudio.nix
  ];

  services.xserver.displayManager.autoLogin = {
    enable = true;
    user = "tvuser";
  };
  services.xserver.displayManager.lightdm.autoLogin.timeout = 5;
  services.xserver.displayManager.defaultSession = "plasma5";
  services.xserver.desktopManager.plasma5.enable = true;

  services.flatpak.enable = true;

  environment.systemPackages = with pkgs; [
    intel-gpu-tools firefox
  ];

  users.users.tvuser = {
    isNormalUser = true;
    extraGroups = [ "networkmanager" "video" ];
  };

  networking.hostName = "nixos-bluecrow";

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "America/Los_Angeles";

  system.stateVersion = "21.05";
}
