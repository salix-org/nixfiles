{ pkgs, ... }:

{
  imports = let
    nixos-hardware = import ../common/nixos-hardware.nix;
  in [
    nixos-hardware.t440p
    nixos-hardware.acpi_call
    ../common/uefi.nix
    ../common/base-standard.nix
    ../common/user.nix
    ../common/xserver.nix
    ../common/i3.nix
    ../common/programs.nix
    ../common/games.nix
    ../common/libvirt.nix
#   ../common/netshare-fs.nix
    ../modules/pulseaudio.nix
    ../modules/thinkfan.nix
    ../modules/netdata.nix
    ../modules/syncthing.nix
  ];

  boot.kernelParams = [
#    "pcie_aspm=force"
    "acpi_osi="
    "fan_control=1"
  ];

  boot.kernel.sysctl = {
    "vm.vfs_cache_pressure" = 50;
    "vm.swappiness" = 10;
    "vm.dirty_background_ratio" = 1;
    "vm.dirty_ratio" = 50;
  };

# fileSystems."/home/jsalcido/altsrc" = {
#   device = "10.244.0.50:/home/jsalcido/altsrc";
#   fsType = "nfs";
#   options = [ "x-systemd.automount" "noauto" "x-systemd.idle-timeout=600" ];
# };

  services.tlp = {
    enable = true;
    settings = {
      "START_CHARGE_THRESH_BAT0" = 70;
      "STOP_CHARGE_THRESH_BAT0" = 90;
#      "CPU_MAX_PERF_ON_AC" = 85;
#      "CPU_MAX_PERF_ON_BAT" = 50;
      "CPU_BOOST_ON_AC" = 1;
      "CPU_BOOST_ON_BAT" = 0;
    };
  };

  virtualisation.podman.enable = true;

  services.mullvad-vpn.enable = true;
  networking.firewall.checkReversePath = "loose";

  environment.systemPackages = with pkgs; [
    virt-manager intel-gpu-tools mullvad-vpn
    (callPackage ../packages/barrier-remote-connect.nix {})
  ];

  networking.hostName = "nixos-t440p";

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "America/Los_Angeles";
}
