{ pkgs, ... }:

{
  imports =
    [
      ../common/uefi.nix
      ../common/base-standard.nix
      ../common/user.nix
      ../common/xserver.nix
      ../common/i3.nix
      ../common/programs.nix
      ../common/games.nix
      ../common/work.nix
      ../common/libvirt.nix
      ../common/netshare-fs.nix
      ../modules/pulseaudio.nix
      ../modules/netdata.nix
      ../modules/syncthing.nix
    ];

  hardware.opengl.extraPackages = with pkgs; [ vaapiVdpau ];

  hardware.steam-hardware.enable = true;

  boot.kernelPackages = pkgs.linuxPackages_xanmod;
  boot.initrd.availableKernelModules = [ "nvidia" ];
  boot.kernelParams = [ "amd_iommu=on" ];
# boot.kernel.sysctl = {
#   "vm.swappiness" = 20;
#   "vm.vfs_cache_pressure" = 50;
#   "vm.max_map_count" = 16777216; # star citizen via lutris
# };

# boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

# systemd.tmpfiles.rules = [
#   "f /dev/shm/looking-glass 0660 jsalcido qemu-libvirtd -"
# ];

# virtualisation.podman.enable = true;
  virtualisation.docker.enable = true;

  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.nvidia.modesetting.enable = true;
  services.xserver = {
    xrandrHeads = [
      {
        output = "DP-4";
        primary = true;
        monitorConfig = ''
          VendorName     "Unknown"
          ModelName      "GBT M27Q"
          HorizSync       243.0 - 243.0
          VertRefresh     48.0 - 165.0
          Option         "DPMS" "false"
        '';
      }
    ];
    screenSection = ''
      Option "metamodes" "DP-2: nvidia-auto-select +5920+0 {rotation=left}, DP-4: 2560x1440_170 +3360+600, DP-0: nvidia-auto-select +1920+0 {rotation=left}, HDMI-0: 1920x1080_60 +0+1480"
    '';
    # TODO figure out why this doesn't disable screen timeout
    serverFlagsSection = ''
      Option "StandbyTime" "0"
      Option "SuspendTime" "0"
      Option "OffTime" "0"
      Option "BlankTime" "30"
    '';
  };

  services.flatpak.enable = true;

  # enable a secrets store
  services.gnome.gnome-keyring.enable = true;

  environment.systemPackages = with pkgs; [
    virt-manager nvtop
  ];

  # open firewall for games and stuff
  networking.firewall.allowedTCPPorts = [ 25565 7778 27015 111 2049 20048 ];
  networking.firewall.allowedUDPPorts = [ 25565 7778 27015 111 2049 20048 ];

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "America/Los_Angeles";

  networking.hostName = "nixos-greencobra";
  
  # NOTE now keeping stateVersion in /etc/nixos/configuration.nix
# system.stateVersion = "20.03";
}
