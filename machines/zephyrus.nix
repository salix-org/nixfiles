{ pkgs, config, lib, ... }:

#let
#  nixos-unstable-pinned = import (builtins.fetchTarball {
#    name = "nixos-unstable_nvidia-535-35_2023-07-16";
#    url = https://github.com/nixos/nixpkgs/archive/6cee3b5893090b0f5f0a06b4cf42ca4e60e5d222.tar.gz;
#    sha256 = "088yi2181nfyxalkg0g84jv4jcmxclihnrcaywfmxqzz61vr8x4f";
#  }) { config.allowUnfree = true; };
#  pinnedKernelPackages = nixos-unstable-pinned.linuxPackages_latest;
#in {
{
  imports =
    [
      (import ../common/nixos-hardware.nix).ga401
      ../common/uefi.nix
      ../common/base-standard.nix
      ../common/user.nix
      ../common/xserver.nix
      ../common/plasma.nix
      ../common/libvirt.nix
      ../common/programs.nix
      ../common/games.nix
      ../modules/pipewire.nix
      ../modules/nvidia.nix
      ../modules/printing.nix
    ];

  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.permittedInsecurePackages = [
    "electron-25.9.0"
  ];
  
# nixpkgs.config.packageOverrides = pkgs: {
#   linuxPackages_latest = pinnedKernelPackages;
#   nvidia_x11 = nixos-unstable-pinned.nvidia_x11;
# };

# boot.kernelPackages = pinnedKernelPackages;

  boot.supportedFilesystems = [ "ntfs" ];

  boot.kernelParams = [ "amd_iommu=on" ];
  boot.kernel.sysctl = {
    "vm.vfs_cache_pressure" = 50;
#   "vm.swappiness" = 10; # nixos-hardware sets this to 1 by default
    "vm.dirty_background_ratio" = 1;
    "vm.dirty_ratio" = 50;
    "vm.max_map_count" = 16777216; # star citizen via lutris
  };

# boot.blacklistedKernelModules = [ "nvidia" "nouveau" ];
# boot.kernelModules = [ "vfio_virqfd" "vfio_pci" "vfio_iommu_type1" "vfio" ];
# boot.extraModprobeConfig = "options vfio-pci ids=10de:1f12,10de:10f9,10de:1ada,10de:1adb";

# hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.beta;

# hardware.nvidia.prime = {
#   reverseSync.enable = true;
#   offload.enable = lib.mkForce false;
# };

  services.tlp = {
    enable = true;
    settings = {
      CPU_SCALING_GOVERNOR_ON_BAT="schedutil";
      CPU_SCALING_GOVERNOR_ON_AC="schedutil";
      CPU_MAX_PERF_ON_AC=95;
      CPU_MAX_PERF_ON_BAT=60;
    };
  };
  services.power-profiles-daemon.enable = false;

  services.touchegg.enable = true;

  hardware.steam-hardware.enable = true;
  programs.steam = {
    remotePlay.openFirewall = true;
  };

  environment.systemPackages = with pkgs; [
    virt-manager lm_sensors radeontop mullvad-vpn spotify discord slack
    r2modman
  ];

  services.flatpak.enable = true;

  services.mullvad-vpn.enable = true;
  networking.firewall.checkReversePath = "loose";
  networking.firewall.enable = false;
  # networking.firewall.allowedTCPPorts = [ 25565 ];
  # networking.firewall.allowedUDPPorts = [ 25565 ];

  networking.hostName = "nixos-zephyrus";
  networking.hostId = "c2208359";

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "America/Los_Angeles";
}
