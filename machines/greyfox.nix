{ pkgs, ... }:

{
  imports = [
    (import ../common/nixos-hardware.nix).precision5530
    ../common/uefi.nix
    ../common/base-standard.nix
    ../common/user.nix
    ../modules/netdata.nix
    ../modules/speedtest-tracker.nix
    ../modules/nginx-gateway.nix
    ../modules/matrix.nix
    ../modules/jellyfin.nix
    ../modules/jenkins.nix
  ];

  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [ intel-media-driver vaapiIntel ];
  };

  zramSwap.enable = true;

  services.samba = {
    enable = true;
    extraConfig = ''
      map to guest = Bad User
    '';
    shares = {
      games = {
        path = "/mnt/data/torrent/misc";
        "read only" = "no";
        "guest ok" = "yes";
        "guest only" = "yes";
      };
    };
  };

  environment.systemPackages = with pkgs; [
    intel-gpu-tools
  ];

  # REMINDER: podman can't be enabled at the same time as kubernetes
  virtualisation.podman.enable = true;
  
  # services access
  networking.firewall.allowedTCPPorts = [
    445
    139
    8112 # deluge
    8118
    7878 # radarr
    6767 # bazarr
    9283
    8123
  ];
  networking.firewall.allowedUDPPorts = [
    137
    138
  ];

  # valheim server
# networking.firewall.allowedTCPPortRanges = [
#   { from = 2456; to = 2457; }
# ];
# networking.firewall.allowedUDPPortRanges = [
#   { from = 2456; to = 2457; }
# ];

  networking.firewall.allowPing = true;
  networking.hostName = "nixos-greyfox";

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "America/Los_Angeles";
}
