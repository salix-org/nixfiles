{ pkgs, ...}:

{
  imports = [
    ../common/uefi.nix
    ../common/base-standard.nix
    ../common/user.nix
    ../modules/syncthing.nix
  ];

  services.udisks2.enable = true;

  virtualisation.podman.enable = true;

  environment.systemPackages = with pkgs; [ udiskie ];

  security.polkit.enable = true;

  networking.firewall = {
    allowPing = true;
    allowedTCPPorts = [
      8384 # allow access to Syncthing console
      25565 # mc java
    ];
    allowedUDPPorts = [
      19132 # mc bedrock
    ];
  };
  networking.hostName = "nixos-redhawk";

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "America/Los_Angeles";
}
