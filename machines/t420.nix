{ pkgs, ... }:

{
  imports =
    [
      "${builtins.fetchGit { url = "https://github.com/NixOS/nixos-hardware.git"; }}/lenovo/thinkpad/t420"
      ../common/uefi.nix
      ../common/base-standard.nix
      ../common/user.nix
      ../common/xserver.nix
      ../common/i3.nix
      ../common/programs.nix
      ../common/games.nix
      ../modules/pulseaudio.nix
      ../modules/syncthing.nix
    ];

  boot.kernel.sysctl = {
    "vm.vfs_cache_pressure" = 50;
    "vm.swappiness" = 10;
    "vm.dirty_background_ratio" = 1;
    "vm.dirty_ratio" = 50;
  };

  services.tlp.settings = {
    "START_CHARGE_THRESH_BAT0" = 70;
    "STOP_CHARGE_THRESH_BAT0" = 90;
    "CPU_BOOST_ON_AC" = 1;
    "CPU_BOOST_ON_BAT" = 0;
  };

  virtualisation.podman.enable = true;

  services.mullvad-vpn.enable = true;
  networking.firewall.checkReversePath = "loose";

  environment.systemPackages = with pkgs; [
    virt-manager intel-gpu-tools mullvad-vpn
    (callPackage ../packages/barrier-remote-connect.nix {})
  ];

  zramSwap.enable = true;

  networking.hostName = "nixos-t420";

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "America/Los_Angeles";

  system.stateVersion = "20.03";
}
