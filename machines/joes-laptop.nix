{ pkgs, ... }:

{
  imports = [
    ../common/base-standard.nix
    ../common/user.nix
    ../common/xserver.nix
    ../modules/pulseaudio.nix
  ];

  boot.loader.grub.device = "/dev/sda";

  services.tlp.settings = {
    "START_CHARGE_THRESH_BAT0" = 70;
    "STOP_CHARGE_THRESH_BAT0" = 90;
  };

  services.xserver = {
    desktopManager.xfce = {
      enable = true;
      enableXfwm = true;
    };
    displayManager.defaultSession = "xfce";
  };

  environment.systemPackages = with pkgs; [
    chromium
  ];
  nixpkgs.config.chromium.enableWideVine = true;
  programs.chromium.enable = true;

  users.users.joecartwright = {
    isNormalUser = true;
    extraGroups = [ "networkmanager" ];
    initialPassword = "";
  };

  zramSwap.enable = true;

  networking.hostName = "nixos-joes-laptop";

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "America/Los_Angeles";

  system.stateVersion = "20.03";
}
