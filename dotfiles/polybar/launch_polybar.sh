#!/bin/sh

echo "Launching polybar..."

(
  flock 200

  pkill polybar
  while pgrep -u $UID -x polybar > /dev/null; do sleep 0.5; done

  # Get network interfaces
  export INTERFACE_WIRED=$(ls /sys/class/net | grep ^e)
  export INTERFACE_BRIDGED=$(ls /sys/class/net | grep ^br)
  if [[ -n "$INTERFACE_BRIDGED" ]]; then
    INTERFACE_WIRED=$INTERFACE_BRIDGED
  fi

  export INTERFACE_WIRELESS=$(ls /sys/class/net | grep ^w)

  # Get tray output
  outputs=$(xrandr --query | grep " connected" | cut -d" " -f1)
  tray_outputs=('DP-4' 'eDP-1' 'LVDS-1' 'eDP')

  for m in $outputs; do
    export MONITOR=$m
    export TRAY_POSITION=none
    export TITLE_MAXLEN=40
      if [[ " ${tray_outputs[@]} " =~ " ${m} " ]]; then
        TRAY_POSITION=right
        TITLE_MAXLEN=100
      fi

    polybar -c /etc/polybar/config.ini --reload default </dev/null >/var/tmp/polybar-$m.log 2>&1 200>&- &
    disown
  done
) 200>/var/tmp/polybar-launch.lock

echo "Finished launching polybar"
