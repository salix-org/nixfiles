{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [ kitty ];
  environment.etc = {
    "xdg/kitty/kitty.conf".source = ../dotfiles/kitty/kitty.conf;
  };
}
