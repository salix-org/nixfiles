{ pkgs, ... }:

# TODO need to figure out how to prevent this overwriting my gateway cert
{
  services.k3s = {
    enable = true;
    role = "server";
  };

  environment.systemPackages = with pkgs; [ kubectl ];

  networking.firewall.allowedTCPPorts = [ 6443 ];
}
