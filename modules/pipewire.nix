{ pkgs, ... }:

{
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
    wireplumber.enable = true;
  };

# services.blueman.enable = true; # this probably belongs in i3.nix

  security.rtkit.enable = true;

  environment.systemPackages = with pkgs; [
    pulseaudio # for pactl
    pulsemixer
  ];
}
