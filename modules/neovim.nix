{ pkgs, ... }:

# TODO convert to using program.neovim

let
  myNeovim = pkgs: pkgs.neovim.override {
    configure = {
      vam.pluginDictionaries = [
        { names = [
          "coc-nvim"
          "coc-highlight"
          "coc-snippets"
          "coc-pairs"
          "coc-json"
          "coc-yaml"
          "coc-css"
          "coc-html"
          "coc-tsserver"
          "coc-eslint"
          "coc-prettier"
          "coc-python"
          "coc-go"
          "ctrlp-vim"
          "vim-nix"
          "vim-lastplace"
          "vim-sensible"
          "vim-polyglot"
          "gitgutter"
          "vim-airline"
          "vim-airline-themes"
          "gruvbox-community"
	      ]; }
      ];
	    customRC = ''
        " configured in neovim.nix

        set hidden
        set cmdheight=2
        set shortmess+=c
        set signcolumn=yes
        set termguicolors
        set updatetime=100
        set relativenumber
        set tabstop=2
        set shiftwidth=2
        set expandtab
        set nowrap
        set sidescroll=5
        set listchars+=precedes:<,extends:>
        set noshowmode
        set nocompatible
        set mouse=a

        let g:gruvbox_italic=1
        colorscheme gruvbox

        let g:airline_theme='murmur'
        let g:airline_powerline_fonts=1

        inoremap <silent><expr> <TAB>
              \ pumvisible() ? coc#_select_confirm() :
              \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump','''])\<CR>" :
              \ <SID>check_back_space() ? "\<TAB>" :
              \ coc#refresh()
        inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

        function! s:check_back_space() abort
          let col = col('.') - 1
          return !col || getline('.')[col - 1]  =~# '\s'
        endfunction

        inoremap <silent><expr> <c-space> coc#refresh()

        nmap <silent> [c <Plug>(coc-diagnostic-prev)
        nmap <silent ]c <Plug>(coc-diagnostic-next)

        nmap <silent> gd <Plug>(coc-definition)
        nmap <silent> gy <Plug>(coc-type-definition)
        nmap <silent> gi <Plug>(coc-implementation)
        nmap <silent> gr <Plug>(coc-references)

        nnoremap <silent> U :call <SID>show_documentation()<CR>

        function! s:show_documentation()
          if (index(['vim','help'], &filetype) >= 0)
            execute 'h '.expand('<cword>')
          else
            call CocAction('doHover')
          endif
        endfunction

        autocmd CursorHold * silent call CocActionAsync('highlight')

        nmap <leader>rn <Plug>(coc-rename)

        command! -nargs=0 Format :call CocAction('format')

        "set statusline^=%{coc#status()}%{get(b:,'coc_current_function',''')}

        augroup Markdown
          autocmd!
          autocmd FileType markdown set wrap
        augroup END
	    '';
    };
  };
in {
# nixpkgs.config.packageOverrides = pkgs: {
#   neovim = myNeovim pkgs;
# };

  environment.variables = rec {
    VISUAL = "nvim";
    EDITOR = VISUAL;
  };

  environment.systemPackages = with pkgs; [
    neovim nodejs go gopls
    (python3.withPackages (ps: with ps; [ pylint jedi black flake8 ]))
  ];
}
