{ pkgs, ... }:

{
  services.jellyfin.enable = true;
  services.jellyfin.package = pkgs.jellyfin;
  users.users.jellyfin.extraGroups = [ "video" "render" ];

  networking.firewall.allowedTCPPorts = [ 8096 ];
}
