{ pkgs, lib, ... }:

let
  nixvim = import (builtins.fetchGit {
    url = "https://github.com/nix-community/nixvim";
  });
in
{
  imports = [
    nixvim.nixosModules.nixvim
  ];

  programs.nixvim = {
    enable = true;

#   colorschemes.gruvbox.enable = true;

    plugins = {
      airline.enable = true;

      lsp = {
        enable = true;
        servers = {
          tsserver.enable = true;
          gopls.enable = true;
          nixd.enable = true;
        };
      };

      cmp = {
        enable = true;
        autoEnableSources = true;
        settings = {
          mapping = {
            "<C-Space>" = "cmp.mapping.complete()";
            "<C-d>" = "cmp.mapping.scroll_docs(-4)";
            "<C-e>" = "cmp.mapping.close()";
            "<C-f>" = "cmp.mapping.scroll_docs(4)";
            "<CR>" = "cmp.mapping.confirm({ select = true })";
            "<S-Tab>" = "cmp.mapping(cmp.mapping.select_prev_item(), {'i', 's'})";
            "<Tab>" = "cmp.mapping(cmp.mapping.select_next_item(), {'i', 's'})";
          };
          snippet = {
            expand = "function(args) require('luasnip').lsp_expand(args.body) end";
          };
          sources = [
            { name = "nvim_lsp"; }
            { name = "luasnip"; }
            { name = "path"; }
            { name = "buffer"; }
          ];
        };
      };
    };

    opts = {
      number = true;
      relativenumber = true;
      cursorline = true;

      splitbelow = true;
      splitright = true;

      tabstop = 2;
      softtabstop = 2;
      shiftwidth = 2;
      expandtab = true;

      scrolloff = 8;

      incsearch = true;
      ignorecase = true;
      smartcase = true;
    };

    keymaps = [
      {
        mode = "n";
        key = "<C-u>";
        options.silent = true;
        action = "<C-u>zz";
      }
      {
        mode = "n";
        key = "<C-d>";
        options.silent = true;
        action = "<C-d>zz";
      }
    ];
  };

  environment.systemPackages = with pkgs; [
    nodejs_21
  ];
}
