let
  username = "jsalcido";
  group = "users";
in {
  services.syncthing = {
    enable = true;
    user = username;
    group = group;
    dataDir = "/home/${username}/syncthing";
    configDir = "/home/${username}/.config/syncthing";
    guiAddress = ":8384";
    openDefaultPorts = true;
  };
}
