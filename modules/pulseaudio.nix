{ pkgs, ... }:

{
  sound.enable = true;
  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
#   extraModules = [ pkgs.pulseaudio-modules-bt ]; # no longer required, see Nixos 22.05 release notes
    support32Bit = true;
    daemon.config = {
      default-sample-format = "s24le";
      default-sample-rate = "48000";
      resample-method = "speex-float-4";
    };
  };

  services.blueman.enable = true;

  environment.systemPackages = with pkgs; [
    pulsemixer
  ];
}
