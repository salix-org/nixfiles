{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [ qutebrowser ];

  environment = {
    variables = {
      BROWSER = "qutebrowser";
    };
    shellAliases = {
      "qbw" = "${pkgs.qutebrowser}/bin/qutebrowser -B ~/.config/qutebrowser-profile/work &!";
      "qbu" = "${pkgs.qutebrowser}/bin/qutebrowser -B ~/.config/qutebrowser-profile/unsafe &!";
    };
  };
}
