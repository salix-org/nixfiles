{ pkgs, ... }:

{
  hardware.bluetooth = {
    enable = true;
    package = pkgs.bluez; # enable all bluez features
  };
}
