{
  nixpkgs.config.packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball {
      url = "https://github.com/nix-community/NUR/archive/eab1122f5267ed9bfbf2c0676908987d15cc78d6.tar.gz";
      sha256 = "0ya3l124fz8s92d4sq3c0cv6mf5g97k47hvkl8m3m2m9s5d3qgmy";
    }) {
      inherit pkgs;
    };
  };
}
