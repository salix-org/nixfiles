{ pkgs, ... }:

let
  ipAddress = "10.244.0.199";
  hostname = "greyfox.salix.dev";
  port = 6443;
in {
  services.kubernetes = {
    roles = [ "master" "node" ];
    masterAddress = hostname;
    apiserverAddress = "https://${hostname}:${toString port}";
    easyCerts = true;
    apiserver = {
      securePort = port;
      advertiseAddress = ipAddress;
    };

    addons.dns.enable = true;

    # needed to allow using swap on host
    kubelet.extraOpts = "--fail-swap-on=false";
  };

  environment.systemPackages = with pkgs; [
    kompose kubectl kubernetes
  ];
}
