# Temporary until https://github.com/NixOS/nixpkgs/pull/110721 is merged into unstable

{
  nixpkgs.config.packageOverrides = pkgs: {
    qemu = pkgs.qemu.overrideAttrs (oldAttrs: {
      postFixup = ''
        # the .desktop is both invalid and pointless
        rm -f $out/share/applications/qemu.desktop
        # copy qemu-ga (guest agent) to separate output
        mkdir -p $ga/bin
        cp $out/bin/qemu-ga $ga/bin/
      '';
    });
  };
}
