{ domain }:

let
  fqdn = "home.${domain}";
in {
  services.nginx.virtualHosts."${fqdn}" = {
    forceSSL = true;
    useACMEHost = domain;

    locations."/jellyfin" = {
      return = "302 $scheme://$host/jellyfin/";
    };
    locations."/jellyfin/" = {
      proxyPass = "http://localhost:8096/jellyfin/";
      proxyWebsockets = true;
      extraConfig = ''
        proxy_buffering off;
      '';
    };
  };

# security.acme.certs."${domain}".extraDomainNames = [ fqdn ];
}
