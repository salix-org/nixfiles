{ pkgs, ... }:

{
  fonts = {
    enableDefaultPackages = true;
    packages = with pkgs; [
      fira-code
      fira-code-symbols
#     (nerdfonts.override {
#       fonts = [ "FiraCode" ];
#     })
    ];
    fontconfig.defaultFonts = {
      serif = [ "FiraCode" ];
      sansSerif = [ "FiraCode" ];
      monospace = [ "FiraCode Nerd Font Mono" ];
    };
  };
}
