let
  port = 10420;
in {
  services.jenkins = {
    enable = true;
    inherit port;
  };

# services.nginx.virtualHosts."home.salix.dev".locations = {
#   "/jenkins" = {
#     return = "302 $scheme://$host/jenkins/";
#   };
#   "/jenkins/" = {
#     proxyPass = "http://localhost:${toString port}/";
#     proxyWebsockets = true;
#   };
# };

  networking.firewall.allowedTCPPorts = [ 10420 ];
}
