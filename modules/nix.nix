{
  nix = {
    settings.experimental-features = [ "nix-command" "flakes" ];
    autoOptimiseStore = true;
    gc = {
      automatic = true;
      dates = "Sat 3:00";
      options = "--delete-older-than 30d";
    };
    extraOptions = ''
      keep-outputs = true
      keep-derivations = true
    '';
  };
}
