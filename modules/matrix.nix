{ pkgs, ...}:

let
  domain = "salix.dev";
  fqdn = "matrix.${domain}";
in {
  services.matrix-synapse = {
    enable = true;
    settings = {
      server_name = fqdn;
      enable_metrics = true;
      enable_registration = false;
      listeners = [
        {
          port = 8008;
          tls = false;
          bind_addresses = [ "127.0.0.1" ];
          x_forwarded = true;
          type = "http";
          resources = [
            {
              names = [ "client" ];
              compress = true;
            }
            {
              names = [ "federation" ];
              compress = false;
            }
          ];
        }
      ];
    };
  };

  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_11;
    initialScript = pkgs.writeText "synapse-init.sql" ''
      CREATE ROLE "matrix-synapse" WITH LOGIN PASSWORD 'synapse';
      CREATE DATABASE "matrix-synapse" WITH OWNER "matrix-synapse"
        TEMPLATE template0
        LC_COLLATE = "C"
        LC_CTYPE = "C";
    '';
  };
}
