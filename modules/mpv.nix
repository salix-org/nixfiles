{ pkgs, ... }:

{
  nixpkgs.overlays = [
    (self: super: {
      mpv = super.mpv-with-scripts.override {
        scripts = [ (pkgs.callPackage ../packages/mpv-youtube-quality.nix {}) ];
      };
    })
  ];

  environment.systemPackages = with pkgs; [
    mpv youtube-dl
  ];

  environment.etc = {
    "mpv/mpv.conf".source = ../dotfiles/mpv/mpv.conf;
    "mpv/input.conf".source = ../dotfiles/mpv/input.conf;
  };

  environment.variables = {
    MPV_HOME = "/etc/mpv";
  };
}
