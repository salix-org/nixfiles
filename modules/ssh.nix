{
  services.openssh = {
    enable = true;
    permitRootLogin = "no";
    forwardX11 = true;
  };

  programs.mosh = {
    enable = true;
    withUtempter = true;
  };
}
