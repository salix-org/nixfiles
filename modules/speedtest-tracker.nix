{
  virtualisation.oci-containers.containers.speedtest-tracker = {
    image = "henrywhitaker3/speedtest-tracker";
    ports = [ "8765:80" ];
    volumes = [ "speedtest-tracker-data:/config" ];
    environment = {
      OOKLA_EULA_GDPR = "true";
    };
  };

  networking.firewall.allowedTCPPorts = [ 8765 ];
}
