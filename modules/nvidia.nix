{ pkgs, ... }:

{
  nixpkgs.config.allowUnfree = true;

  hardware.opengl.enable = true;

  services.xserver.videoDrivers = [ "nvidia" ];

  environment.systemPackages = with pkgs; [ nvtop ];
}
