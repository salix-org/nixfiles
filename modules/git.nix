{ pkgs, ... }:

{
  programs.git.enable = true;

  environment.systemPackages = with pkgs.gitAndTools; [
    git-open delta
  ];

  # TODO use programs.git.config
  environment.etc."gitconfig".text = ''
    [user]
      name = Josiah Salcido
      email = code@salix.dev
    [core]
      editor = nvim
      pager = delta
    [pull]
      rebase = false
    [delta]
      plus-style = "syntax #012800"
      minus-style = "syntax #340001"
      syntax-theme = zenburn
    [interactive]
      diffFilter = delta --color-only
    [init]
      defaultBranch = master
  '';
}
