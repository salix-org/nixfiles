{ pkgs, ... }:

{
  services.nextcloud = {
    enable = true;
    package = (pkgs.callPackage ../packages/nextcloud {});
    hostName = "home.salix.dev";
    https = true;
    config = {
      trustedProxies = [ "192.168.1.252" ];
      adminpassFile = "/var/nextcloud/admin";
    };
  };

  networking.firewall.allowedTCPPorts = [ 80 ];
}
