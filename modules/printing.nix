{ pkgs, ... }:

{
    services.printing = {
        enable = true;
        drivers = with pkgs; [ cnijfilter_4_00 ];
    };
    services.avahi.enable = true;
    services.avahi.nssmdns = true;
}