{ devices }:

{
  boot.initrd = {
    availableKernelModules = [ "vfio-pci" ];
    preDeviceCommands = ''
      devices="${(builtins.concatStringsSep " " devices)}"
      for device in $devices; do
        echo "vfio-pci" > /sys/bus/pci/devices/$device/driver_override
      done
      modprobe -i vfio-pci
    '';
  };
}
