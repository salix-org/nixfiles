{
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    # TCP 27036 27037
    # UDP 27031 27036
  };
}
