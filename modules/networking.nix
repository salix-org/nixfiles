{
  networking = {
    networkmanager = {
      enable = true;
      unmanaged = [ "interface-name:ve-*" ]; # don't manage vm ifaces
    };

    nat.enable = true;
    wireguard.enable = true;
  };

  services.zerotierone.enable = true; # for global remote access

  services.avahi = {
    enable = true;
    nssmdns = true;
    publish = {
      enable = true;
      addresses = true;
    };
  };
}
