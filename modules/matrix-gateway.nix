{ domain }:

let
  fqdn = "matrix.${domain}";
in {
  services.nginx = {
    virtualHosts = {
      "${fqdn}" = {
        forceSSL = true;
        useACMEHost = domain;

        locations."= /.well-known/matrix/server".extraConfig = let
          server = { "m.server" = "${fqdn}:443"; };
        in ''
          add_header Content-Type application/json;
          return 200 '${builtins.toJSON server}';
        '';
  
        locations."= /.well-known/matrix/client".extraConfig = let
          client = {
            "m.homeserver" = { "base_url" = "https://${fqdn}"; };
            "m.identity_server" = { "base_url" = "https://vector.im"; };
          };
        in ''
          add_header Content-Type application/json;
          add_header Access-Control-Allow-Origin *;
          return 200 '${builtins.toJSON client}';
        '';
  
        locations."/".return = "302 https://element.io/";
  
        locations."/_matrix" = {
          proxyPass = "http://127.0.0.1:8008";
        };  
      };
    };
  };

# security.acme.certs."${domain}".extraDomainNames = [ fqdn ];
}
