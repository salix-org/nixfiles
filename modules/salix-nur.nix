{
  nixpkgs.config.packageOverrides = pkgs: {
    salix-nur = import (pkgs.fetchFromGitLab {
      owner = "salix-org";
      repo = "nur";
      rev = "HEAD";
      sha256 = "06dfhzgpzdcs4wwrjwvpai6ki1dkhdz6yi454bnq9p7jnfpxm12b";
    }) {};
  };
}
