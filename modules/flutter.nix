{ pkgs, ... }:

{
#  programs.adb.enable = true; # Flutter requires using platform-tools from Sdk
  services.udev.packages = [
    pkgs.android-udev-rules
  ];

  environment.systemPackages = with pkgs; [
#    flutterPackages.beta  # Use flutter env per project
    android-studio
    jdk8
  ];

  environment.variables = {
    ANDROID_SDK_ROOT = "/home/jsalcido/Android/Sdk";
    CHROME_EXECUTABLE = "${pkgs.chromium}/bin/chromium";
  };
}
