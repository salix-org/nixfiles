{ pkgs, ... }:

{
  programs.zsh = {
    enable = true;
    autosuggestions.enable = true;
    enableCompletion = true;
    syntaxHighlighting.enable = true;
    ohMyZsh = {
      enable = true;
      plugins = [ "git" "colored-man-pages" "command-not-found" ];
    };
    interactiveShellInit = ''
      eval "$(direnv hook zsh)"
    '';
  };

  programs.starship.enable = true;

  users.defaultUserShell = pkgs.zsh;
}
