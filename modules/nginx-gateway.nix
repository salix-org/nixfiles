let
  acmeEmail = "commander225@gmail.com";
  domain = "salix.dev";
in {
  services.nginx = {
    enable = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    virtualHosts = {
      "${domain}" = {
        forceSSL = true;
        enableACME = true;

        locations."/test".extraConfig = ''
          add_header Content-Type text/plain;
          return 200 'Hello world!';
        '';
      };

      # TODO self signed cert, use root to set web dir (avoid home dir due to sandboxing)
      "greyfox.salix.dev" = {
        forceSSL = false;
        enableACME = false;

        locations."/".extraConfig = ''
          add_header Content-Type text/plain;
          return 200 'Test succesful!';
        '';
      };
    };
  };

  imports = [
    (import ./matrix-gateway.nix { inherit domain; })
    (import ./jellyfin-gateway.nix { inherit domain; })
  ];

  security.acme = {
    acceptTerms = true;
    email = acmeEmail;
  };

  networking.firewall.allowedTCPPorts = [ 80 443 ];
  networking.firewall.allowedUDPPorts = [ 80 443 ];
}
